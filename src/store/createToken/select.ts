import { createSelector } from "reselect";

import { State } from "@/store";

import { State as CreateTokenState } from "./reducer";

const createTokenSelector = (state: State): CreateTokenState =>
  state.createToken;

export const createTokenLoadingSelector = createSelector(
  createTokenSelector,
  createToken => createToken.loading
);

export const createTokenSuccessSelector = createSelector(
  createTokenSelector,
  createToken => createToken.success
);
