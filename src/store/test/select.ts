import { createSelector } from "reselect";

import { State } from "@/store";

import { State as TestSate } from "./reducer";

const testSelector = (state: State): TestSate => state.test;

export const testLoadingSelector = createSelector(
  testSelector,
  test => test.loading
);

export const testSuccessSelector = createSelector(
  testSelector,
  test => test.success
);
