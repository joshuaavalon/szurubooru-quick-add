import React from "react";
import ReactDOM from "react-dom";

import "./antd.less";

import { Options } from "@/component";
import { StoreProvider } from "@/store";

import "./style.scss";

ReactDOM.render(
  <StoreProvider>
    <Options />
  </StoreProvider>,
  document.getElementById("root") as HTMLElement
);
