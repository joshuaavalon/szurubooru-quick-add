import { EventType, Pixiv, PixivData } from "@/model";

browser.runtime.onMessage.addListener(request => {
  if (request.action !== "upload") {
    return;
  }
  const scriptTag = document.createElement("script");
  scriptTag.src = browser.extension.getURL("site/pixiv/page.js");
  (document.head || document.documentElement).appendChild(scriptTag);
  scriptTag.onload = () => {
    scriptTag.remove();
  };
});

window.addEventListener("message", (event: MessageEvent) => {
  const { data = {} } = event;
  const { type, payload } = data;
  if (type !== EventType.Pixiv) {
    return;
  }
  const pixivData: PixivData = payload;
  const keys = Object.keys(pixivData.preload.illust);
  if (keys.length !== 1) {
    return;
  }
  const illust = pixivData.preload.illust[keys[0]];
  const {
    title,
    userName: author,
    userId,
    urls,
    pageCount,
    description
  } = illust;
  const authorUrl = `https://www.pixiv.net/member.php?id=${userId}`;
  const url = `https://www.pixiv.net/member_illust.php?mode=medium&illust_id=${
    keys[0]
  }`;
  const imageUrl = urls.original;
  const images: string[] = [];
  for (let i = 0; i < pageCount; i++) {
    const image = imageUrl.replace("_p0", `_p${i}`);
    images.push(image);
  }
  const el = $("<div></div>");
  const regex = /<br\s*[/]?>/giu;
  el.html(description.replace(regex, "\n"));
  const content = el.text();
  const pixiv: Pixiv = {
    title,
    author,
    authorUrl,
    images,
    type: "pixiv",
    url,
    content
  };
  browser.runtime.sendMessage(pixiv);
});
