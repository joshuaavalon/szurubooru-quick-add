import React, { PureComponent } from "react";
import { Layout } from "antd";

export default class Footer extends PureComponent {
  public render(): JSX.Element {
    return (
      <Layout.Footer>
        Szurubooru Quick Add &copy; 2018 Created by Joshua Avalon
      </Layout.Footer>
    );
  }
}
