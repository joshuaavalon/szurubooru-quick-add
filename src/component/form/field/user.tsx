import React, { Component } from "react";
import { FormComponentProps } from "antd/lib/form/Form";

import { StringField } from "./base";
import { ComponentProps } from "./base/string";

export class UserField extends Component<FormComponentProps> {
  private config = {
    fieldId: "userName",
    icon: "user",
    placeholder: "User name",
    required: true,
    message: "User name  is required."
  };

  public render(): JSX.Element {
    const { form } = this.props;
    const fieldProps: ComponentProps = { form, ...this.config };
    return <StringField {...fieldProps} />;
  }
}
