import React, { Component } from "react";
import { FormComponentProps } from "antd/lib/form/Form";

import { TextArea } from "../base";
import { ComponentProps } from "../base/area";

export class TwitterCommentField extends Component<FormComponentProps> {
  private config = {
    fieldId: "twitterComment",
    placeholder: "Comment Template"
  };

  public render(): JSX.Element {
    const { form } = this.props;
    const fieldProps: ComponentProps = { form, ...this.config };
    return <TextArea {...fieldProps} />;
  }
}
