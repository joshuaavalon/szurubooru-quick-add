import React, { Component } from "react";
import { Form, Input } from "antd";
import { FormComponentProps } from "antd/lib/form/Form";

type OwnProps = {
  fieldId: string;
  placeholder?: string;
  required?: boolean;
  message?: string;
};

export type ComponentProps = OwnProps & FormComponentProps;

export class TextArea extends Component<ComponentProps> {
  public render(): JSX.Element {
    const { form, fieldId, required, message, placeholder } = this.props;
    const { getFieldDecorator } = form;
    return (
      <Form.Item>
        {getFieldDecorator(fieldId, {
          rules: [{ required, message }]
        })(<Input.TextArea placeholder={placeholder} />)}
      </Form.Item>
    );
  }
}
