import React, { Component } from "react";
import { Form, Icon, Input } from "antd";
import { FormComponentProps } from "antd/lib/form/Form";

type OwnProps = {
  fieldId: string;
  type?: string;
  icon?: string;
  placeholder?: string;
  required?: boolean;
  message?: string;
};

export type ComponentProps = OwnProps & FormComponentProps;

export class StringField extends Component<ComponentProps> {
  public render(): JSX.Element {
    const {
      form,
      fieldId,
      required,
      message,
      icon,
      placeholder,
      type
    } = this.props;
    const { getFieldDecorator } = form;
    return (
      <Form.Item>
        {getFieldDecorator(fieldId, {
          rules: [{ required, message }]
        })(
          <Input
            prefix={<Icon type={icon} />}
            placeholder={placeholder}
            type={type}
          />
        )}
      </Form.Item>
    );
  }
}
