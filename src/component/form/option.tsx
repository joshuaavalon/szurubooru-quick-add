import React, { Component } from "react";
import { Divider, Form } from "antd";
import { FormComponentProps } from "antd/lib/form/Form";

import {
  PasswordField,
  PixivCommentEnableField,
  PixivCommentField,
  ServerField,
  TokenField,
  TwitterCommentEnableField,
  TwitterCommentField,
  UserField
} from "./field";
import { ServerButtonRow } from "./buttonRow";

export class OptionsForm extends Component<FormComponentProps> {
  public render(): JSX.Element {
    const { form } = this.props;
    return (
      <Form className="szurubooru-form">
        <h2>Szurubooru</h2>
        <ServerField form={form} />
        <UserField form={form} />
        <PasswordField form={form} />
        <TokenField form={form} />
        <ServerButtonRow form={form} />
        <Divider />
        <h2>Twitter</h2>
        <TwitterCommentEnableField form={form} />
        <TwitterCommentField form={form} />
        <h2>Pixiv</h2>
        <PixivCommentEnableField form={form} />
        <PixivCommentField form={form} />
      </Form>
    );
  }
}
