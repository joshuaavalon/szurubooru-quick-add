import Mustache from "mustache";

import { Pixiv, Tweet } from "@/model";
import { Authorization, Post, SzurubooruApi } from "@/api";

enum MenuItem {
  Upload = "upload"
}

type UploadResponse = Tweet | Pixiv;

let api: SzurubooruApi;

async function uploadImage(
  images: string[],
  comment?: string
): Promise<Post[]> {
  const posts: Post[] = [];
  for (const image of images) {
    const post = await api.createPost(image, posts.map(p => p.id));
    posts.push(post);
  }

  browser.notifications
    .create({
      type: "basic",
      iconUrl: browser.extension.getURL("icon/icon-48.png"),
      title: "Szurubooru Quick Add",
      message: "Upload success"
    })
    .then(id => {
      if (id) {
        setTimeout(() => browser.notifications.clear(id), 3000);
      }
    });

  if (!comment || posts.length <= 0) {
    return posts;
  }
  await api.createComment(posts[0].id, comment);
  return posts;
}

async function uploadTwitter(response: Tweet): Promise<Post[]> {
  const { options } = await browser.storage.local.get("options");
  const { twitterTemplateEnable, twitterComment = "" } = options;
  const comment = twitterTemplateEnable
    ? Mustache.render(twitterComment, response)
    : undefined;
  return uploadImage(response.images, comment);
}

async function uploadPixiv(response: Pixiv): Promise<Post[]> {
  const { options } = await browser.storage.local.get("options");
  const { pixivTemplateEnable, pixivComment = "" } = options;
  const comment = pixivTemplateEnable
    ? Mustache.render(pixivComment, response)
    : undefined;
  return uploadImage(response.images, comment);
}

function upload(response: UploadResponse): void {
  switch (response.type) {
    case "twitter":
      uploadTwitter(response);
      break;
    case "pixiv":
      uploadPixiv(response);
      break;
  }
}

browser.storage.local.get("options").then(values => {
  const { apiUrl = "", userName = "", password, token } = values.options;
  const auth = token
    ? Authorization.token(userName, token)
    : Authorization.basic(userName, password);
  api = new SzurubooruApi(apiUrl, auth);
});

browser.contextMenus.create({
  id: MenuItem.Upload,
  title: browser.i18n.getMessage("menuItemUpload"),
  contexts: ["all"],
  documentUrlPatterns: [
    "*://twitter.com/*/status/*",
    "*://www.pixiv.net/member_illust.php*"
  ]
});

browser.contextMenus.onClicked.addListener((info, tab) => {
  if (!tab) {
    return;
  }
  switch (info.menuItemId) {
    case MenuItem.Upload:
      browser.tabs
        .sendMessage(tab.id as number, { action: "upload" })
        .then(upload);
      break;
  }
});

browser.runtime.onMessage.addListener((message: UploadResponse) =>
  upload(message)
);
