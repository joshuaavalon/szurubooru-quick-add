export type WindowEvent = {
  type?: EventType;
  payload?: any;
};

export enum EventType {
  Pixiv = "pixiv"
}
